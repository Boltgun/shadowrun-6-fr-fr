package de.rpgframework.shadowrun6.print.json.model;

import org.prelle.shadowrun6.items.ItemSubType;

import java.util.List;

public class JSONItem {
    public String name;
    public int count;
    public String type;
    public ItemSubType subType;
    public String page;
    public List<JSONItemAccessory> accessories;
}
