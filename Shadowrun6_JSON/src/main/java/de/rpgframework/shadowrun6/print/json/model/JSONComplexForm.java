package de.rpgframework.shadowrun6.print.json.model;

import org.prelle.shadowrun6.ComplexForm;

import java.util.List;

public class JSONComplexForm {
    public String name;
    public String duration;
    public int fading;
    public List<String> influences;
    public String page;
}
