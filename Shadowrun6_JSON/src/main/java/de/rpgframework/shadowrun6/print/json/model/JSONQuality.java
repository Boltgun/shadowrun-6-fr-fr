package de.rpgframework.shadowrun6.print.json.model;

public class JSONQuality {
    public String name;
    public String id;
    public String choice;
    public boolean positive;
    public String page;
}
