package org.prelle.shadowrun6.vehiclegen;

import org.prelle.shadowrun6.vehicle.DesignMod;

/**
 * @author prelle
 *
 */
public class DesignModValue {
	
	private DesignMod ref;
	private int level;
	private int cost;

	//-------------------------------------------------------------------
	public DesignModValue(DesignMod mod) {
		if (mod==null)
			throw new NullPointerException();
		this.ref = mod;
	}

	//-------------------------------------------------------------------
	public DesignMod getDesignMod() {
		return ref;
	}

	//-------------------------------------------------------------------
	public int getLevel() {
		return level;
	}

	//-------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getCost() {
		if (level==0)
			return ref.getBuildPoints();
		return ref.getBuildPoints()*level;
	}

	//-------------------------------------------------------------------
	/**
	 * @param cost the cost to set
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}

}
