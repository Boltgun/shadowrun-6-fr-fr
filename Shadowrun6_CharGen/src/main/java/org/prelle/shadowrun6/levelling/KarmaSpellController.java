/**
 *
 */
package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ChoiceType;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.Tradition;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOption;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOptionType;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.charctrl.SpellController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.modifications.SpellModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaSpellController implements SpellController, CharacterProcessor {

	private static Logger logger = LogManager.getLogger("shadowrun6.lvl.spell");

	protected ProcessorRunner parent;
	private boolean alchemistic;
	protected List<Spell> recSpells;
	protected List<Spell> avSpells;
	protected List<ToDoElement> todos;

	private ShadowrunCharacter model;
	
	//-------------------------------------------------------------------
	/**
	 */
	public KarmaSpellController(ProcessorRunner parent, boolean alchemistic) {
		this.parent = parent;
		this.alchemistic = alchemistic;
		todos      = new ArrayList<>();
		avSpells = new ArrayList<>();
		recSpells = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	protected void updateAvailable() {
		avSpells.clear();
		avSpells.addAll(ShadowrunTools.filterByPluginSelection(ShadowrunCore.getSpells(), model));
		// Remove those the character already has
		for (SpellValue val : model.getSpells(alchemistic)) {
			if (val.getModifyable().canBeLearnedMultiple()) {
				// Spell can be learned multiple times
				continue;
			}
			logger.trace("Not available anymore "+val);
			avSpells.remove(val.getModifyable());
		}

		Collections.sort(avSpells);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#getSpellsLeft()
	 */
	@Override
	public int getSpellsLeft() {
		return -1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#getAvailableSpells()
	 */
	@Override
	public List<Spell> getAvailableSpells(boolean alchemistic) {
		return avSpells;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#getAvailableSpells(org.prelle.shadowrun6.Spell.Category)
	 */
	@Override
	public List<Spell> getAvailableSpells(Spell.Category category, boolean alchemistic) {
		return getAvailableSpells(alchemistic).stream().filter(test -> test.getCategory()==category).collect(Collectors.toList());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#canBeSelected(org.prelle.shadowrun6.Spell)
	 */
	@Override
	public boolean canBeSelected(Spell spell, boolean alchemistic) {
		// Not already selected
		for (SpellValue tmp : model.getSpells(alchemistic)) {
			if (tmp.getModifyable()==spell)
				return false;
		}

		// Enough karma?
		return model.getKarmaFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#canBeSelected(org.prelle.shadowrun6.Spell)
	 */
	@Override
	public boolean canBeDeselected(SpellValue spell) {
		// Minimum requirement is that the spell is known
		if (!model.getSpells().contains(spell))
			return false;
		
		boolean allowUndoCareer   = (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER.getValue();
		boolean allowUndoCreation = (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION.getValue();
		SpellModification mod = getModification(spell);
				
		if (mod==null) {
			return allowUndoCreation;
		} else if (mod.getSource()==this) {
			// This career session - always allowed
			return true;
		} else
			return allowUndoCareer;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#select(org.prelle.shadowrun6.Spell)
	 */
	@Override
	public SpellValue select(Spell spell, boolean alchemistic) {
		if (spell.getChoiceType()!=null) {
			logger.debug("Trying to select a spell that needs a choice with the wrong method");
			throw new IllegalArgumentException("The spell '"+spell.getId()+"' requires a choice");
		}
		if (!canBeSelected(spell,alchemistic)) {
			logger.warn("Trying to select spell "+spell+" which cannot be selected");
			return null;
		}

		int cost = 5;
		logger.debug("select "+spell+" for "+cost+" karma");

		// Change model
		logger.info("Selected spell "+spell);
		SpellValue sVal = new SpellValue(spell);
		sVal.setAlchemistic(alchemistic);
		model.addSpell(sVal);
		

		// Make undoable
		SpellModification mod = new SpellModification(spell);
		mod.setExpCost(cost);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		mod.setAlchemistic(alchemistic);
		model.addToHistory(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Pay money
		boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
		if (payGear) {
			int nuyen = 0;
			switch (spell.getCategory()) {
			case HEALTH: 
			case DETECTION:  
				nuyen=500;	
				break;
			case  ILLUSION: 
				nuyen=1000; 
				break;
			case COMBAT:
				nuyen=2000;
				break;
			case MANIPULATION:
				nuyen=1500;
				break;
			}
			model.setNuyen(model.getNuyen()-nuyen);			
		}
		
		parent.runProcessors();
		return sVal;
	}

	//--------------------------------------------------------------------
	private SelectionOption getOptionFor(ChoiceType choice, SelectionOption...options) {
		for (SelectionOption opt : options) {
			if (choice==ChoiceType.SKILL && opt.getType()==SelectionOptionType.SKILL)
				return opt;
			if (choice==ChoiceType.PHYSICAL_SKILL && opt.getType()==SelectionOptionType.PHYSICAL_SKILL)
				return opt;
			if (choice==ChoiceType.AMMUNITION_TYPE && opt.getType()==SelectionOptionType.AMMOTYPE)
				return opt;
			if (choice==ChoiceType.NAME && opt.getType()==SelectionOptionType.NAME)
				return opt;
		}
		logger.warn("Cannot find selection option for "+choice+" in options "+Arrays.toString(options));
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#select(org.prelle.shadowrun6.Spell)
	 */
	@Override
	public SpellValue select(Spell spell, boolean alchemistic, SelectionOption...options) {
		if (spell.getChoiceType()==null) {
			logger.debug("Trying to select a spell that does not needs a choice with the wrong method");
			throw new IllegalArgumentException("The spell '"+spell.getId()+"' does not require a choice");
		}
		if (!canBeSelected(spell,alchemistic)) {
			logger.warn("Trying to select spell "+spell+" which cannot be selected");
			return null;
		}

		int cost = 5;
		logger.debug("select "+spell+" for "+cost+" karma");

		// Check validity of options
		if (spell.getChoiceType()!=null) {
			SelectionOption option = getOptionFor(spell.getChoiceType(), options);
			if (option==null) {
				logger.warn("Cannot select "+spell+" - missing selection option for choice type "+spell.getChoiceType());
				return null;
			}
		}
		logger.debug("Options: "+Arrays.toString(options));

		// Change model
		logger.info("Selected spell "+spell);
		SpellValue sVal = new SpellValue(spell);
		sVal.setAlchemistic(alchemistic);
		
		// Modify according to options
		for (SelectionOption option : options) {
			switch (option.getType()) {
			case SKILL:
			case PHYSICAL_SKILL:
				sVal.setChoice(option.getAsSkill());
				sVal.setChoiceReference(  option.getAsSkill().getId() );
				break;
			case NAME:
				sVal.setChoice(option.getAsName());
				sVal.setChoiceReference(  option.getAsName() );
				break;
			default:
				logger.error("Unprocessed SelectionOptionType "+option.getType());
				System.err.println("Unprocessed SelectionOptionType "+option.getType());
			}
		}
		model.addSpell(sVal);
		

		// Make undoable
		SpellModification mod = new SpellModification(spell);
		mod.setExpCost(cost);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		mod.setAlchemistic(alchemistic);
		model.addToHistory(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Pay money
		boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
		if (payGear) {
			int nuyen = 0;
			switch (spell.getCategory()) {
			case HEALTH: 
			case DETECTION:  
				nuyen=500;	
				break;
			case  ILLUSION: 
				nuyen=1000; 
				break;
			case COMBAT:
				nuyen=2000;
				break;
			case MANIPULATION:
				nuyen=1500;
				break;
			}
			model.setNuyen(model.getNuyen()-nuyen);			
		}
		
		parent.runProcessors();
		return sVal;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#selectCustom(org.prelle.shadowrun6.Spell)
	 */
	@Override
	public SpellValue selectCustom(Spell data) {
		SpellValue ret = select(data, false);
		if (ret!=null) {
			model.addSpellDefinition(data);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	private SpellModification getModification(SpellValue sVal) {
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof SpellModification))
				continue;
			SpellModification amod = (SpellModification)mod;
			if (amod.getSpell()!=sVal.getModifyable())
				continue;
			if (amod.isAlchemistic()!=sVal.isAlchemistic())
				continue;
			return amod;
		}
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#deselect(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public void deselect(SpellValue ref) {
		logger.debug("deselect "+ref.getModifyable());
		if (!canBeDeselected(ref))
			return;

		model.removeSpell(ref);
		if (model.hasSpellDefinition(ref.getModifyable().getId())) {
			Spell custom = model.getSpellDefinition(ref.getModifyable().getId());
			model.removeSpellDefinition(custom);
		}

		boolean refundCreation = (Boolean)SR6ConfigOptions.REFUND_FROM_CREATION.getValue();
		boolean refundCareer   = (Boolean)SR6ConfigOptions.REFUND_FROM_CAREER.getValue();
		SpellModification mod = getModification(ref);
		
		int cost = 0;
		if (mod==null) {
			// Spell was chosen during creation
			cost = refundCreation?5:0;
		} else if (mod.getExpCost()==0) {
			// Spell was granted for free
		} else if (mod.getSource()==this) {
			// Spell was selected this session
			cost = mod.getExpCost();
		} else {
			// Spell was selected during a previous career session
			cost = refundCareer?5:0;
		}
		logger.debug("Refund "+cost+" karma    - refundCreation="+refundCreation+", refundCareer="+refundCareer);
		
		if (mod!=null) {
			model.removeFromHistory(mod);
		}

		// Grant karma
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);

		// Grant money
		boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
		if (payGear) {
			int nuyen = 0;
			switch (ref.getModifyable().getCategory()) {
			case HEALTH: 
			case DETECTION:  
				nuyen=500;	
				break;
			case  ILLUSION: 
				nuyen=1000; 
				break;
			case COMBAT:
				nuyen=2000;
				break;
			case MANIPULATION:
				nuyen=1500;
				break;
			}
			model.setNuyen(model.getNuyen()-nuyen);			
		}

		parent.runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#changeMagicTradition(org.prelle.shadowrun6.Tradition)
	 */
	@Override
	public void changeMagicTradition(Tradition data) {
		if (model.getTradition()==data)
			return;
		logger.info("Change magic tradition to "+data);
		model.setTradition(data);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList(
				SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER, 
				SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION, 
				SR6ConfigOptions.REFUND_FROM_CAREER, 
				SR6ConfigOptions.REFUND_FROM_CREATION);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.debug("run");
		this.model = model;
		try {			
			// Clear all attribute modifications in character
			todos.clear();
			updateAvailable();
		} finally {
			
		}
		return unprocessed;
	}

}
