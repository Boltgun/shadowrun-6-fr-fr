package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.MartialArts;
import org.prelle.shadowrun6.MartialArtsValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.Technique;
import org.prelle.shadowrun6.TechniqueValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.MartialArtsController;
import org.prelle.shadowrun6.common.CommonMartialArtsController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.modifications.MartialArtsModification;
import org.prelle.shadowrun6.modifications.TechniqueModification;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class MartialArtsLeveller extends CommonMartialArtsController implements MartialArtsController {

	protected static final Logger logger = LogManager.getLogger("shadowrun6.lvl.martial");

	//-------------------------------------------------------------------
	/**
	 */
	public MartialArtsLeveller(CharacterController parent) {
		super(parent);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList(
				SR6ConfigOptions.REFUND_FROM_CAREER, 
				SR6ConfigOptions.REFUND_FROM_CREATION);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#select(org.prelle.shadowrun6.MartialArts)
	 */
	@Override
	public MartialArtsValue select(MartialArts data) {
		MartialArtsValue ret = super.select(data);
		if (ret==null)
			return null;
		
		// Log
		MartialArtsModification mod = new MartialArtsModification(data);
		mod.setExpCost(7);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);
		
		// Pay karma
		model.setKarmaFree(model.getKarmaFree() -7);
		model.setKarmaInvested(model.getKarmaInvested() +7);

		// Pay money
		boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
		if (payGear) {
			model.setNuyen(model.getNuyen()-2500);			
		}
		
		parent.runProcessors();		
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#deselect(org.prelle.shadowrun6.MartialArtsValue)
	 */
	@Override
	public boolean deselect(MartialArtsValue ref) {
		logger.info("deselect "+ref);
		boolean deselected = super.deselect(ref);
		if (!deselected)
			return deselected;
		
		boolean refundFromCareer = (Boolean)SR6ConfigOptions.REFUND_FROM_CAREER.getValue();
		boolean refundFromCreation = (Boolean)SR6ConfigOptions.REFUND_FROM_CREATION.getValue();
		int karma = 0;
		MartialArts key = ref.getMartialArt();
		MartialArtsModification toUndo = getModification(ref);
		if (toUndo!=null) {
			// Decrease from career
			model.removeFromHistory(toUndo);
			karma = toUndo.getExpCost();
			
			if (refundFromCareer) {
				logger.info("Deselecting"+key+" from prev. career session grants "+karma+" karma  ");
				model.setKarmaInvested(model.getKarmaInvested()-karma);
				model.setKarmaFree(model.getKarmaFree()+karma);
			} else {
				logger.info("Removing "+key+" from prev. career session without granting "+karma);
				MartialArtsModification mod = new MartialArtsModification(key);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			}
		} else {
			// Deselect from creation value
			logger.debug("Remove a martial art style that has chosen at creation - invested karma was "+karma);
			if (refundFromCreation) {
				logger.info("Removing "+key+" from creation session granting "+karma+" karma  ");
				model.setKarmaInvested(model.getKarmaInvested()-karma);
				model.setKarmaFree(model.getKarmaFree()+karma);
				// If it granted Karma, log it
				MartialArtsModification mod = new MartialArtsModification(key);
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			} else {
				MartialArtsModification mod = new MartialArtsModification(key);
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			}
		}

		// Inform listener
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#select(org.prelle.shadowrun6.Technique)
	 */
	@Override
	public TechniqueValue select(MartialArtsValue learnIn, Technique data) {
		TechniqueValue ret = super.select(learnIn, data);
		if (ret==null)
			return null;
		
		// Log
		TechniqueModification mod = new TechniqueModification(data, learnIn);
		mod.setExpCost(5);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);
		
		// Pay karma
		model.setKarmaFree(model.getKarmaFree() -5);
		model.setKarmaInvested(model.getKarmaInvested() +5);

		// Pay money
		boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
		if (payGear) {
			model.setNuyen(model.getNuyen()-1500);			
		}
		
		parent.runProcessors();		
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#deselect(org.prelle.shadowrun6.TechniqueValue)
	 */
	@Override
	public boolean deselect(TechniqueValue ref) {
		logger.info("deselect "+ref);
		boolean deselected = super.deselect(ref);
		if (!deselected)
			return deselected;
		
		boolean refundFromCareer = (Boolean)SR6ConfigOptions.REFUND_FROM_CAREER.getValue();
		boolean refundFromCreation = (Boolean)SR6ConfigOptions.REFUND_FROM_CREATION.getValue();
		int karma = 0;
		Technique key = ref.getTechnique();
		TechniqueModification toUndo = getModification(ref);
		if (toUndo!=null) {
			// Decrease from career
			model.removeFromHistory(toUndo);
			karma = toUndo.getExpCost();
			
			if (refundFromCareer) {
				logger.info("Deselecting "+key+" from prev. career session grants "+karma+" karma  ");
				model.setKarmaInvested(model.getKarmaInvested()-karma);
				model.setKarmaFree(model.getKarmaFree()+karma);
			} else {
				logger.info("Removing "+key+" from prev. career session without granting "+karma);
				TechniqueModification mod = new TechniqueModification(key, this);
				mod.setLearnedIn(ref.getMartialArt());
				mod.setDate(new Date(System.currentTimeMillis()));
				mod.setRemoved(true);
				model.addToHistory(mod);
			}
		} else {
			// Deselect from creation value
			logger.debug("Remove a technique that has chosen at creation - invested karma was "+karma);
			if (refundFromCreation) {
				logger.info("Removing "+key+" from creation session granting "+karma+" karma  ");
				model.setKarmaInvested(model.getKarmaInvested()-karma);
				model.setKarmaFree(model.getKarmaFree()+karma);
				// If it granted Karma, log it
				TechniqueModification mod = new TechniqueModification(key);
				mod.setLearnedIn(ref.getMartialArt());
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			} else {
				TechniqueModification mod = new TechniqueModification(key);
				mod.setLearnedIn(ref.getMartialArt());
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			}
		}

		// Inform listener
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		this.model = model;
		try {			
			// Clear all attribute modifications in character
			todos.clear();
			logger.info("Running");
			updateAvailable();
		} finally {
			
		}
		return unprocessed;
	}

}
