package org.prelle.shadowrun6.charctrl;

import org.prelle.shadowrun6.Attribute;

/**
 * @author Stefan Prelle
 *
 */
public interface PriorityAttributeController extends AttributeController {
	
	public boolean canDecreaseAdjust(Attribute key);
	public boolean canIncreaseAdjust(Attribute key);
	public boolean increaseAdjust(Attribute key);
	public boolean decreaseAdjust(Attribute key);
	
	public boolean canDecreaseAttrib(Attribute key);
	public boolean canIncreaseAttrib(Attribute key);
	public boolean increaseAttrib(Attribute key);
	public boolean decreaseAttrib(Attribute key);
	
	public boolean canDecreaseKarma(Attribute key);
	public boolean canIncreaseKarma(Attribute key);
	public boolean increaseKarma(Attribute key);
	public boolean decreaseKarma(Attribute key);

}
