/**
 * 
 */
package org.prelle.shadowrun6.charctrl;

import java.util.List;

import org.prelle.shadowrun6.Ritual;
import org.prelle.shadowrun6.RitualValue;

/**
 * @author prelle
 *
 */
public interface RitualController extends Controller {

	//--------------------------------------------------------------------
	/**
	 * Returns the number of spells left to select
	 */
	public int getRitualsLeft();

	//-------------------------------------------------------------------
	public List<Ritual> getAvailableRituals();

	//-------------------------------------------------------------------
	public boolean canBeSelected(Ritual data);

	//-------------------------------------------------------------------
	public RitualValue select(Ritual data);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(RitualValue data);

	//-------------------------------------------------------------------
	public void deselect(RitualValue data);

}
