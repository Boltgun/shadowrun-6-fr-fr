/**
 * 
 */
package org.prelle.shadowrun6.charctrl;

import java.util.List;

import org.prelle.shadowrun6.Lifestyle;
import org.prelle.shadowrun6.LifestyleOption;
import org.prelle.shadowrun6.LifestyleOptionValue;
import org.prelle.shadowrun6.LifestyleValue;
import org.prelle.shadowrun6.SIN;

/**
 * Con
 * @author prelle
 *
 */
public interface SingleLifestyleBuilder extends Controller {

	public List<Lifestyle> getAvailableLifestyles();

	public boolean canSelectLifestyle(Lifestyle lifestyle);

	public void selectLifestyle(Lifestyle lifestyle);
	

	public List<LifestyleOption> getAvailableOptions();

	public boolean canSelectOption(LifestyleOption option);

	public LifestyleOptionValue selectOption(LifestyleOption option);

	public void deselectOption(LifestyleOptionValue option);


	public void selectSIN(SIN sin);

	public void selectName(String name);

	public void selectDescription(String text);

	public int getLifestyleCost();
	
	public LifestyleValue getResult();
	
}
