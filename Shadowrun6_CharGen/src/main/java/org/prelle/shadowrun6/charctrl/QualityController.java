package org.prelle.shadowrun6.charctrl;

import java.util.List;

import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.QualityValue;

import de.rpgframework.genericrpg.ToDoElement;

public interface QualityController extends Controller {

	//-------------------------------------------------------------------
	public int getNetBonusKarma();

	//-------------------------------------------------------------------
	public List<Quality> getAvailableQualities();

	//-------------------------------------------------------------------
	public boolean canBeSelected(Quality skill);

	//-------------------------------------------------------------------
	public QualityValue select(Quality skill);

	//-------------------------------------------------------------------
	/**
	 * For qualities that require a selection (e.g. exceptional attribute)
	 */
	public QualityValue select(Quality skill, Object choice);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean deselect(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean canBeDecreased(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean increase(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean decrease(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean isRecommended(Quality val);

	//-------------------------------------------------------------------
	public <T> List<T> getChoicesFor(Quality val);

	//-------------------------------------------------------------------
	public int getSelectionCost(Quality val);

	//-------------------------------------------------------------------
	public int getDeselectionCost(QualityValue val);

	//-------------------------------------------------------------------
	public void addToDo(ToDoElement toDoElement);

}