/**
 * 
 */
package org.prelle.shadowrun6.charctrl.modifications;

import java.util.Date;

import org.prelle.shadowrun6.MagicOrResonanceOption;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class AvailableMagicOrResonanceModification implements Modification {
	
	private MagicOrResonanceOption opt; 

	//--------------------------------------------------------------------
	public AvailableMagicOrResonanceModification(MagicOrResonanceOption opt) {
		this.opt = opt;
	}

	//--------------------------------------------------------------------
	public String toString() {
		return opt.getType().getId()+":"+opt.getValue();
	}

	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDate(Date date) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int compareTo(Modification o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Modification clone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getExpCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setExpCost(int expCost) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSource(Object src) {
		// TODO Auto-generated method stub
		
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public MagicOrResonanceOption getOption() {
		return opt;
	}

}
