/**
 * 
 */
package org.prelle.shadowrun6;

import org.prelle.shadowrun6.persist.QualityConverter;
import org.prelle.shadowrun6.persist.SkillConverter;
import org.prelle.simplepersist.AttribConvert;

/**
 * @author Stefan
 *
 */
public class Recommendation {

	@org.prelle.simplepersist.Attribute
	private org.prelle.shadowrun6.Attribute attr;
	@org.prelle.simplepersist.Attribute
	@AttribConvert(SkillConverter.class)
	private Skill skill;
	@org.prelle.simplepersist.Attribute
	@AttribConvert(QualityConverter.class)
	private Quality quality;

	//--------------------------------------------------------------------
	public Recommendation() {
	}

	//--------------------------------------------------------------------
	public String toString() {
		StringBuffer buf = new StringBuffer("Recommend");
		if (attr!=null)
			buf.append(" attr="+attr);
		if (skill!=null)
			buf.append(" skill="+skill);
		if (quality!=null)
			buf.append(" quality="+quality);
		return buf.toString();
	}

	//--------------------------------------------------------------------
	public org.prelle.shadowrun6.Attribute getAttribute() {
		return attr;
	}

	//--------------------------------------------------------------------
	public Skill getSkill() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the quality
	 */
	public Quality getQuality() {
		return quality;
	}

}
