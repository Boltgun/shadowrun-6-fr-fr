/**
 *
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.shadowrun6.requirements.RequirementList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.SelectableItem;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class Focus extends BasePluginData implements Comparable<Focus>, SelectableItem {

	private static Logger logger = LogManager.getLogger("shadowrun6");

	@Attribute
	private String id;
	@Attribute(name="avail")
	private int availabilityPlus;
	@Attribute(name="karma")
	private int bondingMultiplier;
	@Attribute(name="cost")
	private int nuyen;
	@Attribute(name="choice")
	private ChoiceType choice;
	@Element
	private ModificationList modifications;
	@Element(name="requires")
	private RequirementList requirements;

	//--------------------------------------------------------------------
	public Focus() {
		modifications = new ModificationList();
		requirements  = new RequirementList();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	@Override
	public String getTypeId() {
		return "focus";
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("focus."+id);
		} catch (MissingResourceException mre) {
			if (!reportedKeys.contains(mre.getKey())) {
				reportedKeys.add(mre.getKey());
				logger.error("Missing property '"+mre.getKey()+"' in "+i18n.getBaseBundleName());
				if (MISSING!=null)
					MISSING.println(mre.getKey()+"=");
			}
			return id;
		}
	}


	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "focus."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "focus."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Focus o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getNuyenCost() {
		return nuyen;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the selectFrom
	 */
	public ChoiceType getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	public boolean needsChoice() {
		return choice!=null;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the requirements
	 */
	public RequirementList getRequirements() {
		return requirements;
	}

	//-------------------------------------------------------------------
	/**
	 * @param requirements the requirements to set
	 */
	public void setRequirements(RequirementList requirements) {
		this.requirements = requirements;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the availabilityPlus
	 */
	public int getAvailabilityPlus() {
		return availabilityPlus;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the bondingMultiplier
	 */
	public int getBondingMultiplier() {
		return bondingMultiplier;
	}

}
