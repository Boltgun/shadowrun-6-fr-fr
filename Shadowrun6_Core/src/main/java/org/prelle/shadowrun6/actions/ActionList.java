/**
 * 
 */
package org.prelle.shadowrun6.actions;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="actions")
@ElementListUnion({
    @ElementList(entry="action", type=ShadowrunAction.class),
})
public class ActionList extends ArrayList<ShadowrunAction> {

	private static final long serialVersionUID = -4307087686099123762L;

	//-------------------------------------------------------------------
	/**
	 */
	public ActionList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public ActionList(Collection<? extends ShadowrunAction> c) {
		super(c);
	}

}
