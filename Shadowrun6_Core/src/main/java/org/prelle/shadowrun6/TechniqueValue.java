/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.shadowrun6.persist.MartialArtsConverter;
import org.prelle.shadowrun6.persist.TechniqueConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name = "techniqueval")
public class TechniqueValue extends ModifyableImpl implements Modifyable, Comparable<TechniqueValue> {
	
	@Attribute(name="ref")
	@AttribConvert(TechniqueConverter.class)
	private Technique technique;
	@Attribute(name="style")
	@AttribConvert(MartialArtsConverter.class)
	private MartialArts martialArt;
	@Attribute(name="choice")
	private String choiceReference;
	
	private transient Object choice;

	//-------------------------------------------------------------------
	public TechniqueValue() {
		modifications = new ModificationList();
	}

	//-------------------------------------------------------------------
	public TechniqueValue(Technique data, MartialArts learnedIn) {
		this();
		if (data==null) throw new NullPointerException("Technique");
		if (learnedIn==null) throw new NullPointerException("MartialArt style");
		this.technique = data;
		this.martialArt = learnedIn;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return (technique!=null)?technique.getName():"?";
	}

	//-------------------------------------------------------------------
	public Technique getTechnique() {
		return technique;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(TechniqueValue other) {
		try {
			return technique.compareTo(other.getTechnique());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println("TechniqueValue = "+other);
			e.printStackTrace();
		}
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		List<Modification> ret = new ArrayList<>();
//		if (!quality.needsChoice())
//			ret.addAll(quality.getModifications());
//		else
		ret.addAll(modifications);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public Object getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(Object choice) {
		this.choice = choice;
		
		if (choice==null)
			return;
		
//		if (choiceReference==null)
//			System.err.println("Set ref by "+choice+"    // "+choice.getClass());
		if (choice instanceof BasePluginData) {
			choiceReference = ((BasePluginData)choice).getId();
		} else
			choiceReference = String.valueOf(choice);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choiceReference
	 */
	public String getChoiceReference() {
		return choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choiceReference the choiceReference to set
	 */
	public void setChoiceReference(String choiceReference) {
		this.choiceReference = choiceReference.replace("&", "+").replace("<", "").replace(">", "").replace("\"", "'");
	}

	//-------------------------------------------------------------------
	/**
	 * @return the martialArt
	 */
	public MartialArts getMartialArt() {
		return martialArt;
	}

	//-------------------------------------------------------------------
	/**
	 * @param martialArt the martialArt to set
	 */
	public void setMartialArt(MartialArts martialArt) {
		this.martialArt = martialArt;
	}

}
