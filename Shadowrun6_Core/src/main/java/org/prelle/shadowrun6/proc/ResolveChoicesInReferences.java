/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ChoiceType;
import org.prelle.shadowrun6.ComplexFormValue;
import org.prelle.shadowrun6.MetamagicOrEchoValue;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.items.CarriedItem;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * Walk through references (QualityValue, ComplexFormValue ...) and resolve
 * eventually existing choice identifier in their respective object
 * @author prelle
 *
 */
public class ResolveChoicesInReferences implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6");
	
	//-------------------------------------------------------------------
	public ResolveChoicesInReferences() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		logger.info("Resolve choices");
		try {
			/* 
			 * Complex forms
			 */
			for (ComplexFormValue tmp : model.getComplexForms()) {
				if (tmp.getModifyable().needsChoice() && tmp.getChoice()==null) {
					try {
						tmp.setChoice(ShadowrunTools.resolveChoiceType(tmp.getModifyable().getChoice(), tmp.getChoiceReference(), model));
					} catch (Exception e) {
						logger.error("Could not resolve choice '"+tmp.getChoiceReference()+"' of type "+tmp.getModifyable().getChoice(),e);
					}
				}
			}
			/* 
			 * Metamagics/Echoes
			 */
			for (MetamagicOrEchoValue tmp : model.getMetamagicOrEchoes()) {
				logger.debug("METAECHO "+tmp);
				if (tmp.getModifyable().needsChoice() && tmp.getChoice()==null) {
					try {
						if (tmp.getModifyable().getSelectFrom()==ChoiceType.CARRIED_ITEM) {
							CarriedItem item = model.getItem(UUID.fromString(tmp.getChoiceReference()));
							if (item==null) {
								logger.error("Failed resolving carried item: "+tmp.getChoiceReference());								
							} else {
								tmp.setChoice(item);
							}
						} else {
							tmp.setChoice(ShadowrunTools.resolveChoiceType(tmp.getModifyable().getSelectFrom(), tmp.getChoiceReference(), model));
						}
					} catch (Exception e) {
						logger.error("Could not resolve choice '"+tmp.getChoiceReference()+"' of type "+tmp.getModifyable().getSelectFrom(),e);
					}
				}
			}
			/* 
			 * Qualities
			 */
			for (QualityValue tmp : new ArrayList<>(model.getQualities())) {
				logger.debug("Quality "+tmp);
				if (tmp.getModifyable()==null) {
					model.removeQuality(tmp);
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Unknown quality found in your character file.\nIt has been removed.\nIf you think this is a mistake, DON'T SAVE this character.");
					continue;
				}
				if (tmp.getModifyable().needsChoice() && tmp.getChoice()==null) {
					try {
						tmp.setChoice(ShadowrunTools.resolveChoiceType(tmp.getModifyable().getSelect(), tmp.getChoiceReference(), model));
						logger.debug(" resolved "+tmp.getChoiceReference()+" (type "+tmp.getModifyable().getSelect()+") of "+tmp.getModifyable()+" to "+tmp.getChoice());
						if (tmp.getModifyable().getSelect()==ChoiceType.NAME && tmp.getDescription()==null)
							tmp.setDescription(tmp.getChoiceReference());
					} catch (Exception e) {
						logger.error("Could not resolve choice '"+tmp.getChoiceReference()+"' of type "+tmp.getModifyable().getSelect(),e);
					}
				}
			}
			/* 
			 * Gear
			 */
			for (CarriedItem tmp : model.getItemsRecursive(true)) {
				if (tmp.getItem().getChoice()!=null && tmp.getChoice()==null) {
					try {
						tmp.setChoice(ShadowrunTools.resolveChoiceType(tmp.getItem().getChoice(), tmp.getChoiceReference(), model));
						logger.debug(" resolved "+tmp.getChoiceReference()+" (type "+tmp.getItem().getChoice()+") of "+tmp.getItem()+" to "+tmp.getChoice());
					} catch (Exception e) {
						logger.error("Could not resolve choice '"+tmp.getChoiceReference()+"' of type "+tmp.getItem().getChoice(),e);
					}
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
