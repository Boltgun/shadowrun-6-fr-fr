/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.modifications.AttributeModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ApplyAttributeModifications implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc.attrib");
	
	//-------------------------------------------------------------------
	public ApplyAttributeModifications() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process AttributeModifications
			for (Modification tmp : previous) {
				if (tmp instanceof AttributeModification) {
					AttributeModification mod = (AttributeModification)tmp;
					switch (mod.getType()) {
					case MAX:
						unprocessed.add(tmp);
						break;
					default:
						logger.info("Apply "+tmp+" from "+tmp.getSource());
						model.getAttribute( mod.getAttribute() ).addModification(tmp);
					}
				} else
					unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}

		return unprocessed;
	}

}
