/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemAttributeNumericalValue;
import org.prelle.shadowrun6.items.ItemAttributeValue;

/**
 * @author Stefan
 *
 */
public class Persona {
	
	private final static PropertyResourceBundle CORE = ShadowrunCore.getI18nResources();
	
	private Map<ItemAttribute, ItemAttributeValue> attributes;
	private Map<Attribute, AttributeValue> attributes2;
	private String name;
	private int[] monitor;
	
	
	//--------------------------------------------------------------------
	public Persona() {
		attributes = new HashMap<>();
		attributes2= new HashMap<>();
		clear();
	}

	//--------------------------------------------------------------------
	public void setAttribute(ItemAttributeValue value) {
		attributes.put(value.getModifyable(), value);
	}

	//--------------------------------------------------------------------
	public void setAttribute(AttributeValue value) {
		attributes2.put(value.getAttribute(), value);
	}

	//--------------------------------------------------------------------
	public void setName(String value) { this.name = value; }
	public String getName() { return name; }

	//--------------------------------------------------------------------
	public void clear() {
		attributes.clear();
		attributes.put(ItemAttribute.ATTACK, new ItemAttributeNumericalValue(ItemAttribute.ATTACK, 0));
		attributes.put(ItemAttribute.SLEAZE, new ItemAttributeNumericalValue(ItemAttribute.SLEAZE, 0));
		attributes.put(ItemAttribute.DATA_PROCESSING, new ItemAttributeNumericalValue(ItemAttribute.DATA_PROCESSING, 0));
		attributes.put(ItemAttribute.FIREWALL, new ItemAttributeNumericalValue(ItemAttribute.FIREWALL, 0));
		attributes.put(ItemAttribute.DEVICE_RATING, new ItemAttributeNumericalValue(ItemAttribute.DEVICE_RATING, 0));
		attributes.put(ItemAttribute.ATTACK_RATING, new ItemAttributeNumericalValue(ItemAttribute.ATTACK_RATING, 0));
		attributes.put(ItemAttribute.DEFENSE_RATING, new ItemAttributeNumericalValue(ItemAttribute.DEFENSE_RATING, 0));
		attributes.put(ItemAttribute.CONCURRENT_PROGRAMS, new ItemAttributeNumericalValue(ItemAttribute.CONCURRENT_PROGRAMS, 0));

		attributes2.clear();
		attributes2.put(Attribute.INITIATIVE_DICE_MATRIX, new AttributeValue(Attribute.INITIATIVE_DICE_MATRIX, 0));
		attributes2.put(Attribute.INITIATIVE_MATRIX, new AttributeValue(Attribute.INITIATIVE_MATRIX, 0));
	}

	public String dump() {
		StringBuffer buf = new StringBuffer();
		for (ItemAttribute key : ItemAttribute.values()) {
			if (attributes.containsKey(key)) 
				buf.append(String.format("%15s: %s\n", key.name(), attributes.get(key).toString()));
		}
		
		return buf.toString();
	}
	
	//--------------------------------------------------------------------
	public ItemAttributeNumericalValue getAttack() {
		return (ItemAttributeNumericalValue) attributes.get(ItemAttribute.ATTACK);
	}

	//--------------------------------------------------------------------
	public ItemAttributeNumericalValue getSleaze() {
		return (ItemAttributeNumericalValue) attributes.get(ItemAttribute.SLEAZE);
	}

	//--------------------------------------------------------------------
	public ItemAttributeNumericalValue getDataProcessing() {
		return (ItemAttributeNumericalValue) attributes.get(ItemAttribute.DATA_PROCESSING);
	}

	//--------------------------------------------------------------------
	public ItemAttributeNumericalValue getFirewall() {
		return (ItemAttributeNumericalValue) attributes.get(ItemAttribute.FIREWALL);
	}

	//--------------------------------------------------------------------
	public int getDeviceRating() {
		return ((ItemAttributeNumericalValue) attributes.get(ItemAttribute.DEVICE_RATING)).getModifiedValue();
	}

	//--------------------------------------------------------------------
	public int getAttackRating() {
		return ((ItemAttributeNumericalValue) attributes.get(ItemAttribute.ATTACK_RATING)).getModifiedValue();
	}

	//--------------------------------------------------------------------
	public int getDefenseRating() {
		return ((ItemAttributeNumericalValue) attributes.get(ItemAttribute.DEFENSE_RATING)).getModifiedValue();
	}

	//--------------------------------------------------------------------
	public String getInitiativeStringAR() {		
		return Resource.format(CORE, "label.ini", 
				attributes2.get(Attribute.INITIATIVE_MATRIX).getModifiedValue(), 
				attributes2.get(Attribute.INITIATIVE_DICE_MATRIX).getModifiedValue());
	}

	//--------------------------------------------------------------------
	public String getInitiativeStringVR() {		
		System.out.println("Attributes2 = "+attributes2);
		return Resource.format(CORE, "label.ini", 
				attributes2.get(Attribute.INITIATIVE_MATRIX_VR_COLD).getModifiedValue(), 
				attributes2.get(Attribute.INITIATIVE_DICE_MATRIX).getModifiedValue()+1);
	}

	//--------------------------------------------------------------------
	public String getInitiativeStringVRHot() {		
		return Resource.format(CORE, "label.ini", 
				attributes2.get(Attribute.INITIATIVE_MATRIX_VR_HOT).getModifiedValue(), 
				attributes2.get(Attribute.INITIATIVE_DICE_MATRIX).getModifiedValue()+2);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the monitor
	 */
	public int[] getMonitor() {
		return monitor;
	}

	//--------------------------------------------------------------------
	/**
	 * @param monitor the monitor to set
	 */
	public void setMonitor(int[] monitor) {
		this.monitor = monitor;
	}

}
