package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.modifications.EdgeModification;
import org.prelle.shadowrun6.persist.ComplexFormConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class ComplexFormValue extends ModifyableImpl implements Modifyable, Comparable<ComplexFormValue> {

	@Attribute(name="ref")
	@AttribConvert(ComplexFormConverter.class)
	private ComplexForm complexForm;
	@Attribute(name="choice")
	private String choiceReference;
	private transient Object choice;

	//-------------------------------------------------------------------
	public ComplexFormValue() {
	}

	//-------------------------------------------------------------------
	public ComplexFormValue(ComplexForm data) {
		this.complexForm = data;
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (complexForm.needsChoice()) {
			if (choice!=null) {
				switch (complexForm.getChoice()) {
				case ATTRIBUTE:
				case MATRIX_ATTRIBUTE:
					return complexForm.getName()+" ("+((org.prelle.shadowrun6.Attribute)choice).getName()+")";
				case PROGRAM:
					return complexForm.getName()+" ("+((ItemTemplate)choice).getName()+")";
				default:
					System.err.println("Error: ComplexFormValue.getName() for type "+complexForm.getChoice());
					return complexForm.getName()+"(?"+choiceReference+"?)";
				}
			}
			return complexForm.getName()+" (Choice not set)";
		}
		return complexForm.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ComplexFormValue other) {
		return complexForm.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	public ComplexForm getModifyable() {
		return complexForm;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public Object getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(Object choice) {
		this.choice = choice;
		
		if (choice==null)
			return;
		
//		if (choiceReference==null)
//			System.err.println("Set ref by "+choice+"    // "+choice.getClass());
		if (choice instanceof BasePluginData) {
			choiceReference = ((BasePluginData)choice).getId();
		} else
			choiceReference = String.valueOf(choice);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choiceReference
	 */
	public String getChoiceReference() {
		return choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choiceReference the choiceReference to set
	 */
	public void setChoiceReference(String choiceReference) {
		this.choiceReference = choiceReference;
	}
	
	//-------------------------------------------------------------------
	public List<BasePluginData> getInfluences() {
		List<BasePluginData> ret = new ArrayList<>();
		for (Modification mod : modifications) {
//			if (mod instanceof SkillModification) {
//				SkillModification sMod = (SkillModification)mod;
//				if (sMod.getSkill()==skill && sMod.isConditional() && sMod.getModificationType()==ModificationValueType.CURRENT) {
//					if (sMod.getSource()==null) {
//						System.err.println("SkillValue.getInfluences: No source for SkillModification "+sMod);
//					} else if (! (sMod.getSource() instanceof BasePluginData)) {
//						System.err.println("SkillValue.getInfluences: Source of SkillModification "+sMod+" is of type "+sMod.getSource().getClass());
//					} else {
//						ret.add( (BasePluginData)sMod.getSource());
//					}
//				}
//			} else 
			if (mod instanceof EdgeModification) {
				EdgeModification eMod = (EdgeModification)mod;
				if (eMod.getSource()==null) {
					System.err.println("SkillValue.getInfluences: No source for EdgeModification "+eMod);
				} else if (! (eMod.getSource() instanceof BasePluginData)) {
					System.err.println("SkillValue.getInfluences: Source of EdgeModification "+eMod+" is of type "+eMod.getSource().getClass());
				} else {
					ret.add( (BasePluginData)eMod.getSource());
				}
				
			} else {
				LogManager.getLogger("shadowrun6.jfx").warn("Don't know how to deal with modification "+mod.getClass());
			}
		}
		return ret;
	}

}
