/**
 * 
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.shadowrun6.requirements.Requirement;
import org.prelle.shadowrun6.requirements.RequirementList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class MetamagicOrEcho extends BasePluginData implements Comparable<MetamagicOrEcho> {
	
	private static Logger logger = LogManager.getLogger("shadowrun6");
	
	public enum Type {
		ECHO,
		METAMAGIC,
		METAMAGIC_ADEPT
	}
	
	@Attribute
	private String id;
	@Attribute
	private Type type;
	@Attribute(name="max")
	private int max;
	@Attribute(name="select")
	private ChoiceType selectFrom;
	
	@Element
	private RequirementList requires;
	@Element
	private ModificationList modifications;
	
	//-------------------------------------------------------------------
	public MetamagicOrEcho() {
		modifications = new ModificationList();
		requires      = new RequirementList();
	}
	
	//-------------------------------------------------------------------
	public MetamagicOrEcho(String id) {
		this();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		buf.append(id+"\n");
		buf.append(modifications+"\n");
		
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null) 
			return id;
		try {
			return i18n.getString("metamagicOrEcho."+id);
		} catch (MissingResourceException e) {
			if (!reportedKeys.contains(e.getKey())) {
				reportedKeys.add(e.getKey());
				logger.warn(String.format("key missing:    %s   %s", i18n.getBaseBundleName(), e.getKey()));
				if (MISSING!=null) {
					MISSING.println(e.getKey()+"=");
				}
			}
			return id;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "metamagicOrEcho."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "metamagicOrEcho."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MetamagicOrEcho o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	public boolean hasLevels() {
		return max>1;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	public List<Requirement> getRequirements() {
		return requires;
	}

	//--------------------------------------------------------------------
	public ChoiceType getSelectFrom() {
		return selectFrom;
	}

	//--------------------------------------------------------------------
	public boolean needsChoice() {
		return selectFrom!=null;
	}

}
