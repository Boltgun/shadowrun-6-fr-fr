/**
 *
 */
package org.prelle.shadowrun6;

import org.prelle.shadowrun6.persist.MagicOrResonanceTypeConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="magicopt")
public class MagicOrResonanceOption extends PriorityOption {

	@Attribute(required=true)
	@AttribConvert(MagicOrResonanceTypeConverter.class)
	private MagicOrResonanceType type;
	@Attribute
	private int value;

	//-------------------------------------------------------------------
	public MagicOrResonanceOption() {
	}

	//-------------------------------------------------------------------
	public MagicOrResonanceOption(MagicOrResonanceType type, int value) {
		this.type = type;
		this.value  = value;
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (type!=null)
			return type.getId();
		return super.toString();
	}

	//-------------------------------------------------------------------
	public MagicOrResonanceType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(PriorityOption o) {
    	if (o instanceof MagicOrResonanceOption) {
    		MagicOrResonanceOption other = (MagicOrResonanceOption)o;
     		return type.compareTo(other.getType());
    	}
    	return 0;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

}
