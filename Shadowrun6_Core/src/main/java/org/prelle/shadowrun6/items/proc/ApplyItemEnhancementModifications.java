/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemEnhancementValue;
import org.prelle.shadowrun6.modifications.ItemEnhancementModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ApplyItemEnhancementModifications implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");
	
	private String prefix;

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> previous) {
		prefix = indent+model.getItem().getId()+": ";
		List<Modification> unprocessed = new ArrayList<>();

		try {
			for (Modification tmp : previous) {
				if (tmp instanceof ItemEnhancementModification) {
					ItemEnhancementModification mod = (ItemEnhancementModification)tmp;
					ItemEnhancementValue  eVal  = new ItemEnhancementValue(mod.getItemEnhancement(), true);
					model.addAutoEnhancement(eVal);
					logger.debug(prefix+"add auto modificiation "+mod.getItemEnhancement());
				} else
					unprocessed.add(tmp);
			}
		} finally {
			
		}
		return unprocessed;
	}

}
