package org.prelle.shadowrun6.items.proc;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemEnhancementValue;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;
import org.prelle.shadowrun6.modifications.ItemHookModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class GetModificationsFromEnhancements implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	//--------------------------------------------------------------------
	/**
	 */
	public GetModificationsFromEnhancements() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(java.lang.String, org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> unprocessed) {
		String prefix = indent+model.getItem().getId()+": ";
		
		for (ItemEnhancementValue enhancement : model.getEnhancements()) {
			List<Modification> mods = enhancement.getModifications();
			if (!mods.isEmpty()) {
//				logger.warn(prefix+"Modifications from "+enhancement.getModifyable().getId()+" = "+mods);
				for (Modification mod : mods) {
					// Ignore modifications from auto added enhancements, that are
					// not conditional modificationa
					if (enhancement.isAutoAdded()) {
						if (mod instanceof ItemAttributeModification) {
							ItemAttributeModification iaMod = (ItemAttributeModification)mod;
							if (!iaMod.isConditional()) {
								logger.debug(prefix+"Ignore from auto-added "+enhancement.getModifyable().getId()+": "+mod);
								continue;
							}
						} else if (mod instanceof ItemHookModification) {
							ItemHookModification ihMod = (ItemHookModification)mod;
							if (model.getSlot(ihMod.getHook())==null) {
								model.addHook(new AvailableSlot(ihMod.getHook(), ihMod.getCapacity()));
								logger.debug(prefix+"Added slot "+ihMod.getHook()+" with capacity "+ihMod.getCapacity());
							}
							continue;
						} else {
							logger.warn(prefix+"Ignore from auto-added "+enhancement.getModifyable().getId()+": "+mod);
							continue;
						}
					}
					
					Modification newMod = ShadowrunTools.instantiateModification(mod, null, 0);
//					Modification newMod = ShadowrunTools.instantiateModification(mod, enhancement.getChoice(), 0);
					logger.info(prefix+"inject from enhancement: "+newMod);
					unprocessed.add(newMod);
				}
			}
		}
		return unprocessed;
	}

}
