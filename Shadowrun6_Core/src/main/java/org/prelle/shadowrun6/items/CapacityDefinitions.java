package org.prelle.shadowrun6.items;

/**
 * @author prelle
 *
 */
public interface CapacityDefinitions {

	public float BODY_DIV_3 = 999f;
	public float BODY_DIV_2 = 998f;
	public float ARMOR_DIV_4 = 997f;
	public float RATING_MINUS_1 = 996f;

}
