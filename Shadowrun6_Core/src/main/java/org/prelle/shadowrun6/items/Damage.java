/**
 *
 */
package org.prelle.shadowrun6.items;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class Damage extends ItemAttributeNumericalValue implements Cloneable {

	public enum Type {
		PHYSICAL,
		STUN
	}

	public enum WeaponDamageType {
		NORMAL,
		ELECTRICAL,
		FLECHETTE,
	}

	private boolean addStrength;
	private Type type;
	private WeaponDamageType weaponDamageType = WeaponDamageType.NORMAL;

	//--------------------------------------------------------------------
	public Damage() {
		super(ItemAttribute.DAMAGE,0, new ArrayList<Modification>());
	}

	//--------------------------------------------------------------------
	public Damage(int val, boolean addStrength, Type type, WeaponDamageType dType) {
		super(ItemAttribute.DAMAGE,val, new ArrayList<Modification>());
		this.addStrength = addStrength;
		this.type = type;
		this.weaponDamageType = dType;
	}

	//--------------------------------------------------------------------
	public Damage(Damage copy, List<Modification> mods) {
		super(ItemAttribute.DAMAGE, copy.getValue(), mods);
		addStrength = copy.addStrength();
		type        = copy.getType();
		weaponDamageType = copy.getWeaponDamageType();
		modifications.addAll(mods);
	}

	//-------------------------------------------------------------------
 	public Damage clone() {
    		try {
				return (Damage) super.clone();
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		return null;
    }

	//--------------------------------------------------------------------
	public boolean addStrength() {
		return addStrength;
	}

	//--------------------------------------------------------------------
	public int getValue() {
		return Math.round(value);
	}

	//--------------------------------------------------------------------
	public Type getType() {
		return type;
	}

	//--------------------------------------------------------------------
	public String toString() {
		StringBuffer buf = new StringBuffer();
		PropertyResourceBundle res = ShadowrunCore.getI18nResources();
		if (addStrength) {
			buf.append("("+org.prelle.shadowrun6.Attribute.STRENGTH.getShortName()+"/2");
			buf.append("+"+getModifiedValue());
			buf.append(")");
		} else
			buf.append(String.valueOf(getModifiedValue()));

		if (type==Type.PHYSICAL)
			buf.append(Resource.get(res,"damage.physical.short"));
		else
			buf.append(Resource.get(res,"damage.stun.short"));

		if (weaponDamageType!=null && weaponDamageType!=WeaponDamageType.NORMAL) {
			buf.append("("+Resource.get(res,"damage.damagetype."+weaponDamageType.name().toLowerCase())+")");
		}

		return buf.toString();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the addStrength
	 */
	public boolean isAddStrength() {
		return addStrength;
	}

	//--------------------------------------------------------------------
	/**
	 * @param addStrength the addStrength to set
	 */
	public void setAddStrength(boolean addStrength) {
		this.addStrength = addStrength;
	}

	//--------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	//--------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the weaponDamageType
	 */
	public WeaponDamageType getWeaponDamageType() {
		return weaponDamageType;
	}

	//--------------------------------------------------------------------
	/**
	 * @param weaponDamageType the weaponDamageType to set
	 */
	public void setWeaponDamageType(WeaponDamageType weaponDamageType) {
		this.weaponDamageType = weaponDamageType;
	}


}
