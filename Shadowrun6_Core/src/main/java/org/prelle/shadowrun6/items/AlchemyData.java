/**
 *
 */
package org.prelle.shadowrun6.items;

import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.persist.SpellConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author Stefan
 *
 */
@Root(name="alchemy")
public class AlchemyData {

	/**
	 * For which items is the accessory usable
	 */
	@Attribute
	@AttribConvert(SpellConverter.class)
	private Spell spell;
	@Attribute(name="drain")
	private int drain;
	@Attribute(name="trigger")
	private String trigger;

	//--------------------------------------------------------------------
	public AlchemyData() {
//		usewith  = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "AlchemyData(spell="+spell+")";
	}

}
