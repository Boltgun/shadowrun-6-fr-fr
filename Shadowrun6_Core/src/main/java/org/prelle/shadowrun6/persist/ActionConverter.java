package org.prelle.shadowrun6.persist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

public class ActionConverter implements StringValueConverter<ShadowrunAction> {
	
	private final static Logger logger = LogManager.getLogger("shadowrun6.persist");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public ShadowrunAction read(String v) throws Exception {
		ShadowrunAction item = ShadowrunCore.getAction(v);
		if (item==null) {
			logger.error("Unknown action reference: '"+v+"'");
			throw new ReferenceException(ReferenceType.ACTION, v);
		}
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(ShadowrunAction v) throws Exception {
		return v.getId();
	}
	
}