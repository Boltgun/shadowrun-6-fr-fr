package org.prelle.shadowrun6.persist;

import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.simplepersist.StringValueConverter;

public class RatingConverter implements StringValueConverter<Integer> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public Integer read(String v) throws Exception {
		if ("BODY/2".equals(v)) return ItemTemplate.RATING_BODY_DIV_2;
		if ("BODY*2".equals(v)) return ItemTemplate.RATING_BODY_MUL_2;
		return Integer.parseInt(v);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(Integer v) throws Exception {
		if (v==ItemTemplate.RATING_BODY_DIV_2) return "BODY/2";
		if (v==ItemTemplate.RATING_BODY_MUL_2) return "BODY*2";
		return String.valueOf(v);
	}
	
}