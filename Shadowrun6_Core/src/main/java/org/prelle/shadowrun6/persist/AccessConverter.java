package org.prelle.shadowrun6.persist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.prelle.shadowrun6.actions.ShadowrunAction.Access;
import org.prelle.simplepersist.StringValueConverter;

public class AccessConverter implements StringValueConverter<List<Access>> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public List<Access> read(String v) throws Exception {
		v = v.trim();
		List<Access> ret = new ArrayList<Access>();
		
		StringTokenizer tok = new StringTokenizer(v, " /");
		while (tok.hasMoreTokens()) {
			String tmp = tok.nextToken();
			ret.add(Access.valueOf(tmp));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(List<Access> v) throws Exception {
		if (v.isEmpty())
			return null;
		
		StringBuffer buf = new StringBuffer();
		for (Iterator<Access> it = v.iterator(); it.hasNext(); ) {
			buf.append(it.next().name());
			if (it.hasNext())
				buf.append("/");
		}
		
		return buf.toString();
	}
	
}