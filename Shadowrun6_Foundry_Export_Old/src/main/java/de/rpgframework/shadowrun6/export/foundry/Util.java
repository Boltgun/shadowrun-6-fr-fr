package de.rpgframework.shadowrun6.export.foundry;

import org.prelle.shadowrun6.Attribute;

/**
 * @author prelle
 *
 */
public class Util {

	//-------------------------------------------------------------------
	public static String translateAttribute(Attribute attr) {
		switch (attr) {
		case BODY     : return "bod";
		case AGILITY  : return "agi";
		case REACTION : return "rea";
		case STRENGTH : return "str";
		case WILLPOWER: return "wil";
		case LOGIC    : return "log";
		case INTUITION: return "int";
		case CHARISMA : return "cha";
		case EDGE     : return "edg";
		case MAGIC    : return "mag";
		case RESONANCE: return "res";
		default:
			return attr.getShortName().toLowerCase();
		}
	}

}
