/**
 * 
 */
package org.prelle.shadowrun6.jfx.items;

import java.util.Collection;

import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.SelectionModel;

/**
 * @author prelle
 *
 */
public class ItemTemplateSelector extends Control implements IListSelector<ItemTemplate>{
	
	private ItemType[] allowedTypes;
	private ItemSubType[] allowedSubTypes;
	private ObservableList<ItemTemplate> selected;

	//-------------------------------------------------------------------
	public ItemTemplateSelector(ItemType[] allowed, CharacterController control) {
		allowedTypes = allowed;
		selected = FXCollections.observableArrayList();
		
		ItemTemplateSelectorSkin skin = new ItemTemplateSelectorSkin(this, control);
		setSkin(skin);
	}

	//-------------------------------------------------------------------
	public ItemTemplateSelector(ItemType[] allowed, ItemSubType[] subAllowed, CharacterController control) {
		allowedTypes = allowed;
		allowedSubTypes = subAllowed;
		selected = FXCollections.observableArrayList();
		
		ItemTemplateSelectorSkin skin = new ItemTemplateSelectorSkin(this, control);
		setSkin(skin);
	}

	//-------------------------------------------------------------------
	public ItemType[] getAllowedTypes() {
		return allowedTypes;
	}

	//-------------------------------------------------------------------
	public ItemSubType[] getAllowedSubTypes() {
		return allowedSubTypes;
	}

	//-------------------------------------------------------------------
	public ObservableList<ItemTemplate> selectedItemProperty() {
		return selected;
	}

	//-------------------------------------------------------------------
	public Collection<ItemTemplate> getSelectedItems() {
		return selected;
	}

	//-------------------------------------------------------------------
	public ItemTemplate getSelectedItem() { 
		if (selected.isEmpty())
			return null;
		return selected.get(0);
	}

	//-------------------------------------------------------------------
	void setSelectedItems(Collection<ItemTemplate> data) {
		selected.setAll(data);
	}

	//-------------------------------------------------------------------
	ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getSelectionModel()
	 */
	@Override
	public SelectionModel<ItemTemplate> getSelectionModel() {
		return ((ItemTemplateSelectorSkin)getSkin()).getSelectionModel();
	}
	
}
