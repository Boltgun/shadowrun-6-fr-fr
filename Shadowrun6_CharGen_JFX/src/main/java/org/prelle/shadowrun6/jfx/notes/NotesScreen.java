/**
 *
 */
package org.prelle.shadowrun6.jfx.notes;

import java.util.Arrays;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreenPage;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Insets;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author Stefan
 *
 */
/**
 * @author Stefan Prelle
 *
 */
public class NotesScreen extends ManagedScreenPage implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private CharacterController control;

	private TextArea tfNotes;

	//--------------------------------------------------------------------
	/**
	 */
	public NotesScreen(CharacterController control) {
		super(UI.getString("screen.notes.title"));
		this.control = control;

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {

		tfNotes = new TextArea();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		getStyleClass().add("text-body");

		HBox content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(tfNotes);
		HBox.setHgrow(tfNotes, Priority.NEVER);
		//		HBox.setMargin(points, new Insets(0,0,20,0));
		HBox.setMargin(tfNotes  , new Insets(0,0,20,0));
		setContent(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfNotes.textProperty().addListener( (ov,o,n) -> control.getCharacter().setNotes(n));
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(UI.getString("screen.notes.title")+" / "+model.getName());
		tfNotes.setText(model.getNotes());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case NOTES_CHANGED:
			logger.debug("rcv "+event.getType()+"   "+Arrays.toString((int[])event.getValue()));
			break;
		default:
			break;
		}
	}

}
