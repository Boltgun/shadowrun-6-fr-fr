package org.prelle.shadowrun6.jfx;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class ValueField extends HBox {
	public Button dec;
	public Button inc;
	private Label value;
	
	//--------------------------------------------------------------------
	public ValueField() {
		dec = new Button("\uE0C6");
		inc = new Button("\uE0C5");
		value = new Label();
		this.getChildren().addAll(dec, value, inc);
		this.setAlignment(Pos.CENTER_LEFT);
		value.getStyleClass().add("text-subheader");
		dec.getStyleClass().addAll("mini-button");
		dec.setStyle("-fx-font-size: 1.2em");
		inc.getStyleClass().addAll("mini-button");
		inc.setStyle("-fx-font-size: 1.2em");
	}
	
	//--------------------------------------------------------------------
	public ValueField(String text) {
		this();
		value.setText(text);
	}
	
	//--------------------------------------------------------------------
	public void setValue(String txt) {
		this.value.setText(txt);
	}
	
	//--------------------------------------------------------------------
	public void setValue(int val) {
		this.value.setText(String.valueOf(val));
	}

	//--------------------------------------------------------------------
	public int getInt() {
		try {
			return Integer.parseInt(value.getText());
		} catch (NumberFormatException e) {
			return 0;
		}
	}

}