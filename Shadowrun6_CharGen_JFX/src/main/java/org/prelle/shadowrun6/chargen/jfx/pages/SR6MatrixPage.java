package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.GearSection;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.character.CharacterHandle;

/**
 * @author Stefan Prelle
 *
 */
public class SR6MatrixPage extends SR6ManagedScreenPage {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR6Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6MatrixPage.class.getName());

	private GearSection  electronics;
	private GearSection  otherGear;

	//-------------------------------------------------------------------
	public SR6MatrixPage(CharacterController control, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super(control, mode, handle, provider);
		this.setId("shadowrun-matrix");
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.matrix"));

		initComponents();
		expLine = new SR6EquipmentFirstLine(mode, control, provider);
		getCommandBar().setContent(expLine);

		try {
			descrBtnEdit.setVisible(false);
		} catch (Throwable e) {
		}
		refresh();
	}

	//-------------------------------------------------------------------
	private void initElectronics() {
		electronics = new GearSection(UI.getString("section.electronics"), control, provider, ItemType.ELECTRONICS);
		otherGear  = new GearSection(Resource.get(UI,"section.other"), control, provider, ItemType.CHEMICALS, ItemType.SURVIVAL, ItemType.BIOLOGY, ItemType.TOOLS, ItemType.MAGICAL);

		DoubleSection combinedSec = new DoubleSection(electronics, otherGear);
		getSectionList().add(combinedSec);

		// Interactivity
		electronics.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		otherGear.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initElectronics();
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (getManager()==null)
			setManager(provider.getManager());
		try { descrBtnEdit.setVisible(data!=null); } catch (Throwable e) {}
		this.helpData = data;
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.trace("refresh");
		super.refresh();
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.matrix"));

		electronics.refresh();
	}

}
