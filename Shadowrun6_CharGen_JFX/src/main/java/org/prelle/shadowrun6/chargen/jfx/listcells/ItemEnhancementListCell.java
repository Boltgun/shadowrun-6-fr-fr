/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.chargen.jfx.dialogs.EditCarriedItemDialog;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemEnhancement;

import de.rpgframework.ResourceI18N;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class ItemEnhancementListCell extends ListCell<ItemEnhancement> {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(EditCarriedItemDialog.class.getName());

	private final static String NORMAL_STYLE = "equipment-cell";
	private final static String NOT_MET_STYLE = "requirement-not-met-text";

	private EquipmentController control;
	private ShadowrunCharacter model;
	private ItemEnhancement data;
	private CarriedItem embedIn;

	private Label lblName;
	private Label lblPrice;
	private Label lblDesc;
	private VBox layout;

	//-------------------------------------------------------------------
	/**
	 * @param asAccessory The item shall be treated as an accessory
	 */
	public ItemEnhancementListCell(ShadowrunCharacter model, EquipmentController ctrl, CarriedItem embedIn) {
		this.control = ctrl;
		this.embedIn = embedIn;
		this.model   = model;
		lblName = new Label();
		lblName.setStyle("-fx-font-weight: bold;");
		lblName.setMaxWidth(Double.MAX_VALUE);

		Region buffer = new Region();
		buffer.setMaxWidth(Double.MAX_VALUE);

		lblPrice = new Label();
		lblPrice.getStyleClass().add("itemprice-label");

		lblDesc = new Label();
//		lblAvail.setStyle("-fx-pref-width: 4em");
//		lblAvail.getStyleClass().add("itemprice-label");

		HBox line1 = new HBox(lblName, lblPrice);
		HBox.setHgrow(lblName, Priority.ALWAYS);
		
		layout = new VBox();
		layout.getChildren().addAll(line1, lblDesc);
		layout.setMaxWidth(Double.MAX_VALUE);
		this.setOnDragDetected(event -> dragStarted(event));
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(ItemEnhancement item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lblName.setText(((ItemEnhancement)item).getName());			
			lblPrice.setText(item.getPriceString(model));
			lblDesc.setText(
					ResourceI18N.get(UI, "label.size")
					+": "+item.getSize()
					+" \t"
					+ItemAttribute.AVAILABILITY.getShortName()+":"+item.getAvailability().toString()
					);
			
			if (control.canBeModifiedWith(embedIn,item)) {
//				lblDesc.getStyleClass().remove(NOT_MET_STYLE);
				this.setDisabled(false);
			} else {
//				lblDesc.getStyleClass().add(NOT_MET_STYLE);
				this.setDisabled(true);
			}
			setGraphic(layout);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("itemmod:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}


}
