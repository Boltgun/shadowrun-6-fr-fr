/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.util.PropertyResourceBundle;

import org.prelle.rpgframework.jfx.FreePointsNode;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.scene.layout.GridPane;

/**
 * @author prelle
 *
 */
public class WizardPointsPane extends GridPane {

	private static PropertyResourceBundle UI = SR6Constants.RES;
	
	private FreePointsNode lblKarma;
	private FreePointsNode lblPoints;
	private FreePointsNode lblPoints2;

	private boolean showPoints;
	private boolean showPoints2;
	
	//-------------------------------------------------------------------
	public WizardPointsPane(boolean showPoints, boolean showPoints2) {
		this.showPoints = showPoints;
		this.showPoints2 = showPoints2;
		
		initComponents();
		initLayout();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lblPoints = new FreePointsNode();
		lblPoints2 = new FreePointsNode();
		lblKarma  = new FreePointsNode();
		lblPoints.setName(Resource.get(UI, "label.points"));
		lblKarma.setName(Resource.get(UI, "label.karma"));
		lblPoints2.setName(null);
		lblPoints.setPoints(0);
		
//		lblPoints2.setStyle("-fx-text-fill: textcolor-highlight-primary; -fx-font-size: large");
//		lblKarma.setStyle("-fx-text-fill: textcolor-highlight-primary");
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		setVgap(5);
		setHgap(5);
		
		add(lblKarma,  0,0);
		add(lblPoints, 1,0);
		add(lblPoints2,2,0);
		lblPoints.setVisible(showPoints);
		lblPoints2.setVisible(showPoints2);
	}
	
	//-------------------------------------------------------------------
	public void setKarma(int karma) {
		lblKarma.setPoints(karma);
	}
	
	//-------------------------------------------------------------------
	public void setPoints(int value) {
		lblPoints.setPoints(value);
	}
	
	//-------------------------------------------------------------------
	public void setPoints(float value) {
		lblPoints.setPoints(value);
	}
	
	//-------------------------------------------------------------------
	public void setPointsName(String value) {
		lblPoints.setName(value);
	}
	
	//-------------------------------------------------------------------
	public void setPoints2(float value) {
		lblPoints2.setPoints(value);
	}
	
	//-------------------------------------------------------------------
	public void setPoints2Name(String value) {
		lblPoints2.setName(value);
	}

}
