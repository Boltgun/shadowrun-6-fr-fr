/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.listcells;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.UseAs;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.vehicle.DesignMod;
import org.prelle.shadowrun6.vehicle.DesignOption;
import org.prelle.shadowrun6.vehicle.QualityFactor;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class DesignOptionAndModListCell extends ListCell<BasePluginData> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private final static String NORMAL_STYLE = "equipment-cell";
	private final static String NOT_MET_STYLE = "requirement-not-met-text";

	private BasePluginData data;

	private Label lblName;
	private Label lblDesc;
	private HBox layout;

	//-------------------------------------------------------------------
	/**
	 * @param asAccessory The item shall be treated as an accessory
	 */
	public DesignOptionAndModListCell() {
		lblName = new Label();
		lblName.setStyle("-fx-font-weight: bold;");
		lblName.setMaxWidth(Double.MAX_VALUE);
//		lblPrice = new Label();
//		lblPrice.getStyleClass().add("itemprice-label");
		
		lblDesc = new Label();
//		lblAvail.setStyle("-fx-pref-width: 4em");
//		lblAvail.getStyleClass().add("itemprice-label");
		
		layout = new HBox();
		layout.getChildren().addAll(lblName, lblDesc);
		layout.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(lblName, Priority.ALWAYS);
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(BasePluginData item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lblName.setText(item.getName());
			if (item instanceof DesignOption) {
				int bp = ((DesignOption)item).getBuildPoints();
				lblDesc.setText(String.valueOf(bp));
			} else if (item instanceof DesignMod) {
				int bp = ((DesignMod)item).getBuildPoints();
				lblDesc.setText(String.valueOf(bp));
			} else if (item instanceof QualityFactor) {
				float fact = ((QualityFactor)item).getMultiplier();
				if (Math.round(fact)!=fact) {
					lblDesc.setText(String.format("x%.1f", fact));				
				} else
					lblDesc.setText(String.format("x%1d",Math.round(fact)));
			} else if (item instanceof ItemTemplate) {
				ItemTemplate gear = (ItemTemplate)item;
				UseAs useAs = gear.getBestAccessoryUsage(ItemType.ACCESSORY);
				if (useAs==null) {
					logger.error("No ACCESSORY itemtype for "+item);
					System.err.println("No ACCESSORY itemtype for "+item);
					lblDesc.setText("BROKEN");
				} else {
					int bp = (int) (useAs.getCapacity() * 2);
					lblDesc.setText(String.valueOf(bp));
				}
			}
			
			setGraphic(layout);
			
//			if (embedIn!=null) {
//				boolean embeddable = false;
//				for (AvailableSlot slot : embedIn.getSlots()) {
//					boolean canBeEmbedded =
//							item.hasRating()
//							?
//							control.canBeEmbedded(embedIn, item, slot.getSlot(), new SelectionOption(SelectionOptionType.RATING, item.hasRating()?item.getMinimumRating():1))
//							:
//							control.canBeEmbedded(embedIn, item, slot.getSlot());
//					if (canBeEmbedded) {
//						embeddable = true;
//					}
//				}
//				setDisabled(!embeddable);
//			}
		}
	}

}
