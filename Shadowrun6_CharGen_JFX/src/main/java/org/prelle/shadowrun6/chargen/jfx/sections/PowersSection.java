package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.PowerValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ShadowrunJFXUtils;
import org.prelle.shadowrun6.jfx.fluent.SelectorWithHelp;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class PowersSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle RES = SR6Constants.RES;

	private CharacterController ctrl;
	
	private Label ptsToSpend;
	private Label ptsTotal;
	private Button btnDec;
	private Button btnInc;
	private ListView<AdeptPowerValue> list;
	private Button btnAdd;
	private Button btnDel;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public PowersSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(),  null);
		if (provider==null)
			throw new NullPointerException("Missing ScreenManagerProvider");
		this.ctrl = ctrl;
		list = new ListView<AdeptPowerValue>();
		initComponents();
		initLayout();
		initInteractivity();
		
		refresh();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		ptsToSpend = new Label();
		ptsToSpend.getStyleClass().add("base");
		ptsToSpend.setStyle("-fx-font-size: 133%");
		ptsTotal = new Label();
		ptsTotal.setStyle("-fx-font-size: 133%");
		btnDec = new Button("\uE0C6");
		btnDec.getStyleClass().addAll("mini-button");
		btnInc = new Button("\uE0C5");
		btnInc.getStyleClass().addAll("mini-button");

		todos = FXCollections.observableArrayList();
		btnAdd = new Button(null, new SymbolIcon("add"));
		btnDel = new Button(null, new SymbolIcon("delete"));
		setDeleteButton(btnDel);
		setAddButton(btnAdd);
		btnDel.setDisable(true);
		list.setCellFactory(new Callback<ListView<AdeptPowerValue>, ListCell<AdeptPowerValue>>() {
			public ListCell<AdeptPowerValue> call(ListView<AdeptPowerValue> param) {
				PowerValueListCell cell =  new PowerValueListCell(ctrl.getPowerController(), list, getManagerProvider());
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.getPowerController().deselect(cell.getItem());
				});
				return cell;
			}
		});
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Label lbToSpend = new Label(Resource.get(RES,"adeptpowerssection.tospend"));
		Label lbFrom = new Label(" "+Resource.get(RES,"adeptpowerssection.of")+" ");
		HBox line = new HBox(20, lbToSpend, ptsToSpend, lbFrom, btnDec, ptsTotal, btnInc);
		line.setAlignment(Pos.CENTER_LEFT);
		if (ctrl.getMode()==CharGenMode.LEVELING) {
			line.getChildren().removeAll(btnDec, btnInc);
		}
		
		VBox layout = new VBox(20, line, list);
		layout.setStyle("-fx-padding: 0.4em");
		setContent(layout);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> onAdd());
		getDeleteButton().setOnAction(ev -> onDelete());
		
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			btnDel.setDisable(n==null || !ctrl.getPowerController().canBeDeselected(n));
			if (n!=null)
				showHelpFor.set(n.getModifyable());
		});
		
		btnDec.setOnAction(ev -> ctrl.getPowerController().decreasePowerPoint());
		btnInc.setOnAction(ev -> ctrl.getPowerController().increasePowerPoints());
	}

	//-------------------------------------------------------------------
	private void onAdd() {
		logger.debug("opening power selection dialog");
		
		PowerSelector selector = new PowerSelector(ctrl.getPowerController());
		selector.setPlaceholder(new Label(RES.getString("adeptpowerssection.selector.placeholder")));
		SelectorWithHelp<AdeptPower> pane = new SelectorWithHelp<>(selector);
		ManagedDialog dialog = new ManagedDialog(RES.getString("adeptpowerssection.selector.title"), pane, CloseType.OK, CloseType.CANCEL);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.info("Closed with "+close);
		if (close==CloseType.OK) {
			AdeptPower data = selector.getSelectionModel().selectedItemProperty().get();
			logger.debug("Selected power: "+data);
			if (data!=null) {
				if (data.needsChoice()) {
					Platform.runLater(new Runnable() {
						public void run() {
							logger.debug("To select "+data+", a further choice is necessary");
							Object selection = ShadowrunJFXUtils.choose(ctrl, getManagerProvider(), ctrl.getCharacter(), data.getSelectFrom(), data.getName());
							ctrl.getPowerController().select(data, selection);
						}});
				} else {
					ctrl.getPowerController().select(data);
				}
				refresh();
			}
		}
	}

	//-------------------------------------------------------------------
	private void onDelete() {
		AdeptPowerValue selected = list.getSelectionModel().getSelectedItem();
		logger.debug("Power to deselect: "+selected);
		ctrl.getPowerController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	public MultipleSelectionModel<AdeptPowerValue> getSelectionModel() {
		return list.getSelectionModel();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		todos.clear();
		todos.addAll(ctrl.getPowerController().getToDos());
		
		ptsToSpend.setText( String.valueOf( ctrl.getPowerController().getPowerPointsLeft()));
		ptsTotal.setText( String.valueOf( ctrl.getCharacter().getPowerPoints()));
		btnDec.setDisable(!ctrl.getPowerController().canDecreasePowerPoints());
		btnInc.setDisable(!ctrl.getPowerController().canIncreasePowerPoints());
		list.getItems().clear();
		list.getItems().addAll(ctrl.getCharacter().getAdeptPowers());
	}

}
