/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.panels;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Ritual;
import org.prelle.shadowrun6.RitualValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.RitualController;
import org.prelle.shadowrun6.chargen.jfx.listcells.RitualListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.RitualValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class RitualSelectionPane extends HBox {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(RitualSelectionPane.class.getName());
	
	private RitualController ctrl;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private ListView<Ritual> lvAvailable;
	private ListView<RitualValue> lvSelected;
	private Label hdAvailable;
	private Label hdSelected;
	private Button btnDelKnow;

	private ObjectProperty<Ritual> showHelpFor = new SimpleObjectProperty<Ritual>();

	//-------------------------------------------------------------------
	public RitualSelectionPane(RitualController ctrl, ScreenManagerProvider provider) {
		this.ctrl = ctrl;
		this.provider = provider;

		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		hdAvailable = new Label(Resource.get(UI, "label.available"));
		hdSelected  = new Label(Resource.get(UI, "label.selected"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdSelected.getStyleClass().add("text-subheader");
		hdAvailable.setStyle("-fx-font-size: 120%");
		hdSelected.setStyle("-fx-font-size: 130%");

		/* Column 1 */
		lvAvailable = new ListView<Ritual>();
		lvAvailable.setCellFactory(new Callback<ListView<Ritual>, ListCell<Ritual>>() {
			public ListCell<Ritual> call(ListView<Ritual> param) {
				RitualListCell cell =  new RitualListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.select(cell.getItem());
				});
				return cell;
			}
		});
		
		/* Column 2 */
		btnDelKnow = new Button("\uE0C6");
		btnDelKnow.getStyleClass().add("mini-button");
		btnDelKnow.setDisable(false);

		lvSelected  = new ListView<RitualValue>();
		lvSelected.setCellFactory(new Callback<ListView<RitualValue>, ListCell<RitualValue>>() {
			public ListCell<RitualValue> call(ListView<RitualValue> param) {
				RitualValueListCell cell =  new RitualValueListCell(ctrl);
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.deselect(cell.getItem());
				});
				return cell;
			}
		});
		
		Label phAvailable = new Label(Resource.get(UI,"placeholder.available"));
		Label phSelected  = new Label(Resource.get(UI,"placeholder.selected"));
		phAvailable.setWrapText(true);
		phSelected.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
		lvSelected.setPlaceholder(phSelected);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		lvAvailable.setStyle("-fx-pref-width: 24em;");
		lvSelected .setStyle("-fx-pref-width: 17em");

		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		Region grow1 = new Region();
		grow1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow1, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, lvAvailable);

		VBox column2 = new VBox(10);
		VBox.setVgrow(lvSelected, Priority.ALWAYS);
		Region grow2 = new Region();
		grow2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow2, Priority.ALWAYS);
		HBox lineKnow = new HBox(5, grow2, btnDelKnow);
		column2.getChildren().addAll(hdSelected, lineKnow, lvSelected);

		column1.setMaxHeight(Double.MAX_VALUE);
		column2.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);
		lvSelected .setMaxHeight(Double.MAX_VALUE);

		getChildren().addAll(column1, column2);

		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelpFor.set(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelpFor.set((n!=null)?n.getModifyable():null));
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(ctrl.getAvailableRituals());

		lvSelected.getItems().clear();
		if (model!=null)
			lvSelected.getItems().addAll(model.getRituals());

		lvSelected.setOnDragDropped(event -> dragFromRitualDropped(event));
		lvSelected.setOnDragOver(event -> dragFromRitualOver(event));
		lvAvailable.setOnDragDropped(event -> dragFromRitualValueDropped(event));
		lvAvailable.setOnDragOver(event -> dragFromRitualValueOver(event));
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	private void dragFromRitualDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("spell")) {
					Ritual data = ShadowrunCore.getRitual(tail);
					if (data==null) {
						logger.warn("Cannot find quality for dropped quality id '"+tail+"'");						
					} else {
						ctrl.select(data);
					} // if data==null else
				}
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private Ritual getRitualByEvent(DragEvent event) {
		if (!event.getDragboard().hasString())
			return null;
	
		String enhanceID = event.getDragboard().getString();
		int pos = enhanceID.indexOf(":");
		if (pos>0) {
			String head = enhanceID.substring(0, pos);
			String tail = enhanceID.substring(pos+1);
			if (head.startsWith("spell")) {
				return ShadowrunCore.getRitual(tail);
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragFromRitualOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			Ritual qual = getRitualByEvent(event);
			if (qual!=null) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	private void dragFromRitualValueDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			Ritual data = getRitualByEvent(event);
			if (data==null) {
				logger.warn("Cannot find quality for dropped quality");						
//			} else if (!data.isFreeSelectable()) {
//				logger.warn("Cannot deselect dropped racial quality id");						
			} else {
				event.setDropCompleted(success);
				event.consume();
				if (model.hasRitual(data.getId())) {
					RitualValue ref = model.getRitual(data.getId());
					ctrl.deselect(ref);
				}
				return;
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragFromRitualValueOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			Ritual qual = getRitualByEvent(event);
			if (qual!=null) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}
	
	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<Ritual> showHelpForProperty() {
		return showHelpFor;
	}

}
